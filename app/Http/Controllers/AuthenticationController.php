<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\Input;
use PHPUnit\Framework\Constraint\Exception;

class AuthenticationController extends Controller
{

    // Default route for displaying form
    public function create()
    {
        return view('registerAndLogin');
    }

    // Logs the user out
    public function destroy(Request $request)
    {
        Auth::logout();
        return Redirect::to('registerAndLogin')->with('message', 'Successfully logged out.');
    }

    // activates user account if confirmation code is valid
    public function confirm($confirmationCode)
    {

        try {

            if (!$confirmationCode) {
                throw new Exception("Confirmation code variable is null.");
            }

            $user = User::whereConfirmationCode($confirmationCode)->first();

            if (!$user) {
                throw new Exception("User variable is null.");
            }

            // 'activate the account'
            $user->confirmed = 1;
            $user->confirmation_code = null;
            $user->save();

            return Redirect::to('userInfo');

        } catch (Exception $e) {
            Log::error($e->getMessage());
        }

    }

    // validates user input from form and proceeds to register or login
    public function checkUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users',
            'password' => 'required|min:5'
        ]);

        // failed validation means wrong input or a login attempt
        if ($validator->fails()) {

            $errors = $validator->errors();

            // check if login attempt
            if ($errors->has('email')) {
                $check = self::tryLogin($request);

                // Input details match database entry
                if ($check) {
                    $email = $request->only('email');
                    $user = User::where('email', $email)->first();
                    Auth::login($user);
                    return Redirect::to('userInfo');

                // Input details didn't match
                } elseif (!$check) {
                    return Redirect::to('registerAndLogin')
                        ->withErrors($validator)
                        ->withInput();
                }
            } else {
                return Redirect::to('registerAndLogin')
                    ->withErrors($validator)
                    ->withInput();
            }
        } else {

            $result = self::createUser($request);

            $confirmationCodeArray = array($result['confirmationCode']);

            // send an account activation email and pass the confirmation code
            self::sendMail($confirmationCodeArray, $result['email']);

            Auth::login($result['user']);

            return Redirect::to('/userInfo');
        }
    }

    // creates a new user from given input
    protected function createUser($request) 
    {
            // if no errors and input is valid then create a new user
            $email = implode($request->only('email'));
            $password = implode($request->only('password'));
            $hashedPassword = Hash::make($password);

            // generate a confirmation code
            $confirmationCode = str_random(30);

            $user = User::create([
                'email' => $email,
                'password' => $hashedPassword,
                'confirmation_code' => $confirmationCode
            ]);

            // collect everything to be returned
            $result = array(
                'email' => $email,
                'confirmationCode' => $confirmationCode,
                'user' => $user
            );

            return $result;
    }

    // sends an email after registration
    protected function sendMail($confirmationCodeArray, $email) 
    {
            Mail::send('email.verify', ['confirmation_code' => $confirmationCodeArray], function ($message) use ($email) {
                $message
                    ->to($email)
                    ->subject('Account activation');
            });
    }

    // tries to log the user in instead of registering a new user
    protected function tryLogin(Request $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

}
