<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

 // Prompts app to send activation emails
Auth::routes(['verify' => true]);

Route::get('/', function () {
    return view('introduction');
})->name('introduction');

// Register, login, logout route logic
Route::get('/registerAndLogin', 'AuthenticationController@create')
    ->name('registerAndLogin');
Route::post('registerAndLogin', 'AuthenticationController@checkUser');
Route::get('/logout', 'AuthenticationController@destroy')
    ->name('logout');

// Confirmation route
Route::get('registerAndLogin/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'AuthenticationController@confirm'
]);

// user information route logic
Route::get('/userInfo', function () {
    return view('userInfo');
})->middleware('auth')
    ->name('userInfo');