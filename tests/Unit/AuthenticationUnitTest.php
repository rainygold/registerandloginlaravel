<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationUnitTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Tests for AuthenticationController functions.
     *
     * @return void
     */

     // copies logic for user registration process and checks validity
    public function testUserRegister()
    {
        // simulate fake user details
        $email = 'testeremail@test.com';
        $password = 'testpassword';
        $hashedPassword = Hash::make($password);
        $confirmationCode = str_random(30);

        // insert the user
        $user = User::create([
            'email' => $email,
            'password' => $hashedPassword,
            'confirmation_code' => $confirmationCode
        ]);

        // check if the user exists within the db (without activation)
        $this->assertDatabaseHas('users', [
            'email' => $email
        ]);
    }

    // checks the confirm function with non-null input
    public function testConfirmValid() 
    {
        // simulate user details
        $user = factory(\App\User::class)->create();
        $confirmationCode = $user->confirmation_code;

        // find the user in the db
        $userInDb = User::whereConfirmationCode($confirmationCode)->first();

        // copied account activation logic
        $userInDb->confirmed = 1;
        $userInDb->confirmation_code = null;
        $userInDb->save();

        $this->assertDatabaseHas('users', [
            'email' => $userInDb->email,
            'confirmed' => 1
        ]);
    }


    // checks if a user can log in after registering
    public function testUserLogin()
    {
        // simulate user details
        $user = factory(\App\User::class)->create();

        // log the user out as creation auto-logins
        Auth::logout($user);

        // log them back in 
        Auth::login($user);

        // check their remember token to confirm a login
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'remember_token' => $user->remember_token
        ]);

        // check that the token isn't null
        $this->assertTrue($user->remember_token != null);
    }

}
