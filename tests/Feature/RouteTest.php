<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreatesApplication;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

class RouteTest extends BaseTestCase
{

    use RefreshDatabase;
    use CreatesApplication;
    public $baseUrl = 'http://localhost';

    /**
     * Tests for web.php.
     *
     * @return void
     */

     // check that root leads to introduction view
    public function testIntroRoute()
    {
        $this->visit('/')
            ->seeRouteIs('introduction');
    }

    // user can see register/login button if not logged in
    public function testIntroLinksNotAuth()
    {
        $this->visit('/')
            ->click('Register/Login')
            ->seeRouteIs('registerAndLogin');
    }

    // user can see user info button if logged in
    public function testIntroLinksAuth()
    {
        $user = factory(\App\User::class)->create();

        $this->actingAs($user)
            ->visit('/')
            ->click('User Information')
            ->seeRouteIs('userInfo');
    }
}
