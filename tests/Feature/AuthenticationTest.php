<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreatesApplication;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

class AuthenticationTest extends BaseTestCase
{

    use CreatesApplication;
    use RefreshDatabase;
    public $baseUrl = 'http://localhost';

    /**
     * Tests for AuthenticationController.
     *
     * @return void
     */

     // check if a user can register (with valid input)
     public function testUserRegistration() {

        $this->visit('/registerAndLogin')
            ->type('testemail@test.com', 'email')
            ->type('testpassword', 'password')
            ->press('Submit')
            ->seeRouteIs('userInfo');
     }

     // check if a user can register (with invalid input)
     public function testUserRegistrationInvalid() {

        $this->visit('/registerAndLogin')
            ->type('testemail@test.com', 'email')
            ->type('test', 'password')
            ->press('Submit')
            ->seeRouteIs('registerAndLogin');
     }

     // check if a logged in user can see their information
     public function testSeeUserInformation() {

        $this->visit('/registerAndLogin')
        ->type('testemail@test.com', 'email')
        ->type('testpassword', 'password')
        ->press('Submit')
        ->seeRouteIs('userInfo')
        ->see('User id:')
        ->see('User email:')
        ->see('Account status:');    
     }

     // check if a user can log out
     public function testUserLogout() {

        $this->visit('/registerAndLogin')
         ->type('testemail@test.com', 'email') 
         ->type('testpassword', 'password')
         ->press('Submit')
         ->seeRouteIs('userInfo')
         ->click('Logout')
         ->seeRouteIs('registerAndLogin');
     }

     // check if a user can activate their account
     public function testActivateAccount() {

      $user = factory(\App\User::class)->create();

      $this->actingAs($user)
         ->visit('/registerAndLogin' . '/' . $user->confirmation_code)
         ->see('Activated');
     }
}
