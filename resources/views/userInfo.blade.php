@extends('layouts.main')
@extends('layouts.header')

@section('content')
<div id="card-container">
    <div class="card" id="laravel-logo">
        <img src="https://upload.wikimedia.org/wikipedia/commons/3/3d/LaravelLogo.png" class="card-img-top" alt="Laravel logo">
        <div class="card-body">
            <h5 class="card-title">User Information</h5>
            <p class="card-text">
                <ul>
                    <li>User id: {{ Auth::id() }}</li>
                    <li>User email: {{ Auth::user()->email }}</li>
                    <li>Account status: @if (Auth::user()->confirmed) Activated @else Unactivated @endif </li>
                </ul>
            </p>
                  <a href="{{ route('introduction') }}" class="card-link">Introduction</a>
                  <a href="{{ route('logout') }}" class="card-link">Logout</a>
        </div>
    </div>
</div>

@endsection