@extends('layouts.main')
@extends('layouts.header')

@section('content')
<div id="card-container">
    <div class="card" id="laravel-logo">
        <img src="https://upload.wikimedia.org/wikipedia/commons/3/3d/LaravelLogo.png" class="card-img-top" alt="Laravel logo">
        <div class="card-body">
            <h5 class="card-title">Welcome</h5>
            <p class="card-text">This is a basic website that provides users with the ability to register, log in, and
                view user information. This
                application was created using the PHP focused Laravel framework.
            </p>

               @auth
                  <a href="{{ route('userInfo') }}" class="card-link">User Information</a>
               @else
                  <a href="{{ route('registerAndLogin') }}" class="card-link">Register/Login</a>
               @endif

        </div>
    </div>
</div>

@endsection