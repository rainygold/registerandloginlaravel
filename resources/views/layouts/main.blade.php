    @yield('header')

    <body>
        <div class = "content-container">
            @yield('content')
        </div>
    </body>
</html>
