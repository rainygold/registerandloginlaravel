## registerAndLogin Project

- PHP version: 7.2.10-0ubuntu1
- Laravel version: 5.7
- Test suite: phpunit
- Additional composer packages: guzzlehttp, browser-kit-testing
- Database: MySQL Ver 14.14 Distrib 5.7.24
- Mail service: Mailtrap

## Setup

- Install MySQL
- Change .env to suit personal config
- php artisan serve
- go to configured localhost address